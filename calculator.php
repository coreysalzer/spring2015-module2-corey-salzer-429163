<!DOCTYPE HTML>
<html>
   <head>
	<title> Super Cool Calculator </title>
   </head>
   <body>
	 <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="GET">
	    <p>
	       <label>First Operand:</label>
	       <input type="text" name="firstnumber" />
	    </p>
	    <p>
	       <input type="radio" name="operation" value="addition"/>Addition
	       <input type="radio" name="operation" value="subtraction"/>Subtraction
	       <input type="radio" name="operation" value="multiplication"/>Multiplication
	       <input type="radio" name="operation" value="division"/>Division
	    </p>
	    <p>
	       <label>Second Operand:</label>
	       <input type="text" name="secondnumber" />
	    </p>
	    <p>
	       <button type="submit" value="Calculate">Calculate</button> 
	    </p>
	 </form>
	 <?php
	    $firstNumber = $_GET['firstnumber'];
	    $secondNumber = $_GET['secondnumber'];
	    $operation = $_GET['operation'];
	    if(isset($firstNumber) && isset($secondNumber) && isset($operation)) {
	       if($firstNumber.is_numeric() || $firstNumber === "0" && $secondNumber.is_numeric() || $secondNumber === "0") {
		  switch($operation) {
		     case "addition":
			$value = "Answer: " . ($firstNumber + $secondNumber);
			break;
		     case "subtraction":
			$value = "Answer: " . ($firstNumber - $secondNumber);
			break;
		     case "multiplication":
			$value = "Answer: " . ($firstNumber*$secondNumber);
			break;
		     case "division":
			if($secondNumber === "0") {
			   $value = "Cannot Divide by Zero";
			}
			else {
			   $value = "Answer: " . ($firstNumber/$secondNumber);
			}
			break;
		     default:
			$value = "There was an error with your calculation";
			break;
		  }
	       }
	       else {
		  $value = "Please input a valid number into each box.";
	       }
	    }
	    else {
	       $value = "Please input a valid number into each box, and choose an operation.";
	    }
	    echo $value;
	 ?>
   </body>
</html>
